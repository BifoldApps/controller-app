function makeMenu($){
  multilevelMenu(1, ctgLists, ctgData);

  var cleanHTML = html.substring(4, html.length - 10);
  //write.error(cleanHTML);
  //document.getElementById("dynamicList").children[1].innerHTML = cleanHTML;
  //console.log(cleanHTML)
  $('#dynamicList').append(cleanHTML);
}

function multilevelMenu(parentId, ctgLists, ctgData)
{
  html = '';
  if(ctgLists[parentId])
  {
    html = '<ul>';

    for(var i in ctgLists[parentId])
    {
      if(ctgLists[ctgLists[parentId][i]])
      {
        html += '<li><span>' + ctgData[ctgLists[parentId][i]].lname +'</span>';
      }
      else
      {
        html += '<li><a href="' + ctgData[ctgLists[parentId][i]].lurl + '">' + ctgData[ctgLists[parentId][i]].lname +'</a>';
      }

      html += multilevelMenu(ctgLists[parentId][i], ctgLists, ctgData);     // re-calls the function to find parent with child-items recursively

      html += '</li>';      // close LI
    };

    html += '</ul>';       // close UL
  };

  return html;
};

var ctgLists = {
  1: [2, 26, 46, 47, 206, 207, 238, 243],
  2: [3, 12, 25],
  3: [4, 5],
  5: [6, 9],
  6: [7, 8],
  9: [10, 11],
  12: [13, 14, 15, 20],
  15: [16, 17, 18, 19],
  20: [21, 22, 23, 24],
  26: [27, 42],
  27: [28, 31, 34],
  28: [29, 30],
  31: [32, 33],
  34: [35, 38, 41],
  35: [36, 37],
  38: [39, 40],
  42: [43],
  43: [44, 45],
  47: [48],
  48: [49, 50, 64, 93, 115, 191],
  50: [51, 56, 61],
  51: [52, 53, 54, 55],
  56: [57, 58, 59, 60],
  61: [62, 63],
  64: [65, 69, 73, 77, 81, 85, 89],
  65: [66, 67, 68],
  69: [70, 71, 72],
  73: [74, 75, 76],
  77: [78, 79, 80],
  81: [82, 83, 84],
  85: [86, 87, 88],
  89: [90, 91, 92],
  93: [94, 97, 100, 103, 106, 109, 112],
  94: [95, 96],
  97: [98, 99],
  100: [101, 102],
  103: [104, 105],
  106: [107, 108],
  109: [110, 111],
  112: [113, 114],
  115: [116, 131, 146, 161, 176],
  116: [117, 118, 119, 120, 123, 126],
  120: [121, 122],
  123: [124, 125],
  126: [127, 128, 129, 130],
  131: [132, 133, 134, 135, 138, 141],
  135: [136, 137],
  138: [139, 140],
  141: [142, 143, 144, 145],
  146: [147, 148, 149, 150, 153, 156],
  150: [151, 152],
  153: [154, 155],
  156: [157, 158, 159, 160],
  161: [162, 163, 164, 165, 168, 171],
  165: [166, 167],
  168: [169, 170],
  171: [172, 173, 174, 175],
  176: [177, 178, 179, 180, 183, 186],
  180: [181, 182],
  183: [184, 185],
  186: [187, 188, 189, 190],
  191: [192, 193, 194, 195, 198, 201],
  195: [196, 197],
  198: [199, 200],
  201: [202, 203, 204, 205],
  207: [208],
  208: [209, 223, 229, 230, 231],
  209: [210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222],
  223: [224, 225, 226, 227, 228],
  231: [232, 235],
  232: [233, 234],
  235: [236, 237],
  238: [239, 240, 241, 242]
};

// object with link data
var ctgData = {
  1: {
    'lname': 'Menu',
    'lurl': '',
  },
  2: {
    'lname': 'Settings',
    'lurl': '',
  },
  3: {
    'lname': 'Response Tuning',
    'lurl': '',
  },
  4: {
    'lname': 'Auto Tune',
    'lurl': 'IO.html',
  },
  5: {
    'lname': 'Manual Tune',
    'lurl': '',
  },
  6: {
    'lname': 'Deadzone',
    'lurl': '',
  },
  7: {
    'lname': 'Open',
    'lurl': '',
  },
  8: {
    'lname': 'Close',
    'lurl': '',
  },
  9: {
    'lname': 'Hysteresis',
    'lurl': '',
  },
  10: {
    'lname': 'Open',
    'lurl': '',
  },
  11: {
    'lname': 'Close',
    'lurl': '',
  },
  12: {
    'lname': 'Stepping Operation',
    'lurl': '',
  },
  13: {
    'lname': 'Open',
    'lurl': 'http://coursesweb.net/javascript/trivia-game-script_s2',
  },
  14: {
    'lname': 'Close',
    'lurl': 'http://coursesweb.net/javascript/trivia-game-script_s2',
  },
  15: {
    'lname': 'Open',
    'lurl': 'http://coursesweb.net/php-mysql/',
  },
  16: {
    'lname': 'Step Type',
    'lurl': 'http://coursesweb.net/php-mysql/lessons',
  },
  17: {
    'lname': 'Time On',
    'lurl': 'http://coursesweb.net/php-mysql/strings',
  },
  18: {
    'lname': 'Step Number',
    'lurl': 'http://coursesweb.net/php-mysql/constants',
  },
  19: {
    'lname': 'Time Off',
    'lurl': 'http://coursesweb.net/php-mysql/arrays',
  },
  20: {
    'lname': 'Close',
    'lurl': 'http://coursesweb.net/php-mysql/pdo-introduction-connection-database',
  },
  21: {
    'lname': 'Step Type',
    'lurl': 'http://coursesweb.net/php-mysql/tutorials_t',
  },
  22: {
    'lname': 'Time On',
    'lurl': 'http://coursesweb.net/php-mysql/uploading-multiple-files_t',
  },
  23: {
    'lname': 'Step Number',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t',
  },
  24: {
    'lname': 'Time Off',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr1_0',
  },
  25: {
    'lname': 'Clock',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr12_11',
  },
  26: {
    'lname': 'Calibration',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr13_12',
  },
  27: {
    'lname': 'Valve Limits',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr13_12',
  },
  28: {
    'lname': 'Close Position',
    'lurl': 'http://coursesweb.net/ajax/',
  },
  29: {
    'lname': 'Command',
    'lurl': 'http://coursesweb.net/ajax/tutorials_t',
  },
  30: {
    'lname': 'Feedback',
    'lurl': 'http://coursesweb.net/ajax/download_l2',
  },
  31: {
    'lname': 'Open Position',
    'lurl': 'http://coursesweb.net/html/',
  },
  32: {
    'lname': 'Command',
    'lurl': 'http://coursesweb.net/css/',
  },
  33: {
    'lname': 'Feedback',
    'lurl': 'http://www.marplo.net/',
  },
  34: {
    'lname': 'Update Calibration',
    'lurl': 'http://www.marplo.net/javascript/',
  },
  35: {
    'lname': 'Close Position',
    'lurl': 'http://www.marplo.net/javascript/lectii-js',
  },
  36: {
    'lname': 'Command',
    'lurl': 'http://www.marplo.net/javascript/sintaxajs.html',
  },
  37: {
    'lname': 'Feedback',
    'lurl': 'http://www.marplo.net/javascript/functii1.html',
  },
  38: {
    'lname': 'Open Position',
    'lurl': 'http://www.marplo.net/javascript/curs-jquery-tutoriale-js',
  },
  39: {
    'lname': 'Command',
    'lurl': 'http://www.marplo.net/javascript/jquery-css-js',
  },
  40: {
    'lname': 'Feedback',
    'lurl': 'http://www.marplo.net/javascript/jquery-css-js#setcss',
  },
  41: {
    'lname': 'Save Calibration',
    'lurl': 'http://www.marplo.net/javascript/jquery-css-js#arclass',
  },
  42: {
    'lname': 'Valve Feedback',
    'lurl': 'http://www.marplo.net/javascript/jquery-css-js#ceclass',
  },
  43: {
    'lname': 'Retransmission Signal',
    'lurl': 'http://www.marplo.net/javascript/jquery-fadein-fadeout-js',
  },
  44: {
    'lname': 'Close',
    'lurl': 'http://www.marplo.net/javascript/jquery-fadein-fadeout-js',
  },
  45: {
    'lname': 'Open',
    'lurl': 'http://www.marplo.net/javascript/jquery-fadein-fadeout-js#fadeto',
  },
  46: {
    'lname': 'Event Log',
    'lurl': 'http://www.marplo.net/javascript/tutoriale-js',
  },
  47: {
    'lname': 'Application',
    'lurl': 'http://coursesweb.net/',
  },
  48: {
    'lname': 'I/O Setup',
    'lurl': 'http://coursesweb.net/javascript/',
  },
  49: {
    'lname': 'Feedback',
    'lurl': 'http://coursesweb.net/javascript/tutorials_t',
  },
  50: {
    'lname': 'Solenoid Outputs',
    'lurl': 'http://coursesweb.net/javascript/settimeout-setinterval_t',
  },
  51: {
    'lname': 'Solenoid Output 1',
    'lurl': 'http://coursesweb.net/javascript/dynamic-variables-javascript_t',
  },
  52: {
    'lname': 'Solenoid Operation',
    'lurl': 'http://coursesweb.net/javascript/validate-radio-checkbox-buttons_t',
  },
  53: {
    'lname': 'Solenoid Type',
    'lurl': 'http://coursesweb.net/jquery/jquery-course',
  },
  54: {
    'lname': 'PWM Holding',
    'lurl': 'http://coursesweb.net/jquery/jquery-basics',
  },
  55: {
    'lname': 'PWM Voltage',
    'lurl': 'http://coursesweb.net/jquery/jquery-basics#selelm',
  },
  56: {
    'lname': 'Solenoid Output 2',
    'lurl': 'http://coursesweb.net/jquery/jquery-ajax',
  },
  57: {
    'lname': 'Solenoid Operation',
    'lurl': 'http://coursesweb.net/jquery/slidedown-slideup',
  },
  58: {
    'lname': 'Solenoid Type',
    'lurl': 'http://coursesweb.net/javascript/javascript-scripts_s2',
  },
  59: {
    'lname': 'PWM Holding',
    'lurl': 'http://coursesweb.net/javascript/trivia-game-script_s2',
  },
  60: {
    'lname': 'PWM Voltage',
    'lurl': 'http://coursesweb.net/javascript/trivia-game-script_s2',
  },
  61: {
    'lname': 'Solenoid Output 3',
    'lurl': 'http://coursesweb.net/php-mysql/',
  },
  62: {
    'lname': 'Solenoid Operation',
    'lurl': 'http://coursesweb.net/php-mysql/lessons',
  },
  63: {
    'lname': 'Solenoid Type',
    'lurl': 'http://coursesweb.net/php-mysql/strings',
  },
  64: {
    'lname': 'Digital Inputs',
    'lurl': 'http://coursesweb.net/php-mysql/constants',
  },
  65: {
    'lname': 'Digital Input 1',
    'lurl': 'http://coursesweb.net/php-mysql/arrays',
  },
  66: {
    'lname': 'Signal',
    'lurl': 'http://coursesweb.net/php-mysql/pdo-introduction-connection-database',
  },
  67: {
    'lname': 'Healthy Status',
    'lurl': 'http://coursesweb.net/php-mysql/tutorials_t',
  },
  68: {
    'lname': 'Signal Length',
    'lurl': 'http://coursesweb.net/php-mysql/uploading-multiple-files_t',
  },
  69: {
    'lname': 'Digital Input 2',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t',
  },
  70: {
    'lname': 'Signal',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr1_0',
  },
  71: {
    'lname': 'Healthy Status',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr12_11',
  },
  72: {
    'lname': 'Signal Length',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr13_12',
  },
  73: {
    'lname': 'Digital Input 3',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr13_12',
  },
  74: {
    'lname': 'Signal',
    'lurl': 'http://coursesweb.net/ajax/',
  },
  75: {
    'lname': 'Healthy Status',
    'lurl': 'http://coursesweb.net/ajax/tutorials_t',
  },
  76: {
    'lname': 'Signal Length',
    'lurl': 'http://coursesweb.net/ajax/download_l2',
  },
  77: {
    'lname': 'Digital Input 4',
    'lurl': 'http://coursesweb.net/html/',
  },
  78: {
    'lname': 'Signal',
    'lurl': 'http://coursesweb.net/css/',
  },
  79: {
    'lname': 'Healthy Status',
    'lurl': 'http://www.marplo.net/',
  },
  80: {
    'lname': 'Signal Length',
    'lurl': 'http://www.marplo.net/javascript/',
  },
  81: {
    'lname': 'Digital Input 5',
    'lurl': 'http://www.marplo.net/javascript/lectii-js',
  },
  82: {
    'lname': 'Signal',
    'lurl': 'http://www.marplo.net/javascript/sintaxajs.html',
  },
  83: {
    'lname': 'Healthy Status',
    'lurl': 'http://www.marplo.net/javascript/functii1.html',
  },
  84: {
    'lname': 'Signal Length',
    'lurl': 'http://www.marplo.net/javascript/curs-jquery-tutoriale-js',
  },
  85: {
    'lname': 'Digital Input 6',
    'lurl': 'http://www.marplo.net/javascript/jquery-css-js',
  },
  86: {
    'lname': 'Signal',
    'lurl': 'http://www.marplo.net/javascript/jquery-css-js#setcss',
  },
  87: {
    'lname': 'Healthy Status',
    'lurl': 'http://www.marplo.net/javascript/jquery-css-js#arclass',
  },
  88: {
    'lname': 'Signal Length',
    'lurl': 'http://www.marplo.net/javascript/jquery-css-js#ceclass',
  },
  89: {
    'lname': 'Digital Input 7',
    'lurl': 'http://www.marplo.net/javascript/jquery-fadein-fadeout-js',
  },
  90: {
    'lname': 'Signal',
    'lurl': 'http://www.marplo.net/javascript/jquery-fadein-fadeout-js',
  },
  91: {
    'lname': 'Healthy Status',
    'lurl': 'http://www.marplo.net/javascript/jquery-fadein-fadeout-js#fadeto',
  },
  92: {
    'lname': 'Signal Length',
    'lurl': 'http://coursesweb.net/',
  },
  93: {
    'lname': 'Digital Outputs',
    'lurl': 'http://coursesweb.net/javascript/',
  },
  94: {
    'lname': 'Digital Output 1',
    'lurl': 'http://coursesweb.net/javascript/tutorials_t',
  },
  95: {
    'lname': 'Signal',
    'lurl': 'http://coursesweb.net/javascript/settimeout-setinterval_t',
  },
  96: {
    'lname': 'Healthy Status',
    'lurl': 'http://coursesweb.net/javascript/dynamic-variables-javascript_t',
  },
  97: {
    'lname': 'Digital Output 2',
    'lurl': 'http://coursesweb.net/javascript/validate-radio-checkbox-buttons_t',
  },
  98: {
    'lname': 'Signal',
    'lurl': 'http://coursesweb.net/jquery/jquery-course',
  },
  99: {
    'lname': 'Healthy Status',
    'lurl': 'http://coursesweb.net/jquery/jquery-basics',
  },
  100: {
    'lname': 'Digital Output 3',
    'lurl': 'http://coursesweb.net/jquery/jquery-basics#selelm',
  },
  101: {
    'lname': 'Signal',
    'lurl': 'http://coursesweb.net/jquery/jquery-basics#selelm',
  },
  102: {
    'lname': 'Healthy Status',
    'lurl': 'http://coursesweb.net/jquery/jquery-ajax',
  },
  103: {
    'lname': 'Digital Output 4',
    'lurl': 'http://coursesweb.net/javascript/javascript-scripts_s2',
  },
  104: {
    'lname': 'Signal',
    'lurl': 'http://coursesweb.net/javascript/trivia-game-script_s2',
  },
  105: {
    'lname': 'Healthy Status',
    'lurl': 'http://coursesweb.net/javascript/trivia-game-script_s2',
  },
  106: {
    'lname': 'Digital Output 5',
    'lurl': 'http://coursesweb.net/php-mysql/',
  },
  107: {
    'lname': 'Signal',
    'lurl': 'http://coursesweb.net/php-mysql/lessons',
  },
  108: {
    'lname': 'Healthy Status',
    'lurl': 'http://coursesweb.net/php-mysql/strings',
  },
  109: {
    'lname': 'Digital Output 6',
    'lurl': 'http://coursesweb.net/php-mysql/constants',
  },
  110: {
    'lname': 'Signal',
    'lurl': 'http://coursesweb.net/php-mysql/arrays',
  },
  111: {
    'lname': 'Healthy Status',
    'lurl': 'http://coursesweb.net/php-mysql/pdo-introduction-connection-database',
  },
  112: {
    'lname': 'Digital Output 7',
    'lurl': 'http://coursesweb.net/php-mysql/tutorials_t',
  },
  113: {
    'lname': 'Signal',
    'lurl': 'http://coursesweb.net/php-mysql/uploading-multiple-files_t',
  },
  114: {
    'lname': 'Healthy Status',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t',
  },
  115: {
    'lname': 'Analogue Inputs',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr1_0',
  },
  116: {
    'lname': 'Analogue Input 1',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr12_11',
  },
  117: {
    'lname': 'Signal',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr13_12',
  },
  118: {
    'lname': 'Signal Type',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr13_12',
  },
  119: {
    'lname': 'Units',
    'lurl': 'http://coursesweb.net/ajax/',
  },
  120: {
    'lname': 'Signal Limits',
    'lurl': 'http://coursesweb.net/ajax/tutorials_t',
  },
  121: {
    'lname': 'Minimum',
    'lurl': 'http://coursesweb.net/ajax/download_l2',
  },
  122: {
    'lname': 'Maximum',
    'lurl': 'http://coursesweb.net/html/',
  },
  123: {
    'lname': 'Actual Limits',
    'lurl': 'http://coursesweb.net/css/',
  },
  124: {
    'lname': 'Minimum',
    'lurl': 'http://www.marplo.net/',
  },
  125: {
    'lname': 'Maximum',
    'lurl': 'http://www.marplo.net/javascript/',
  },
  126: {
    'lname': 'Alarm Limits',
    'lurl': 'http://www.marplo.net/javascript/lectii-js',
  },
  127: {
    'lname': 'Trip HH',
    'lurl': 'http://www.marplo.net/javascript/sintaxajs.html',
  },
  128: {
    'lname': 'Alarm H',
    'lurl': 'http://www.marplo.net/javascript/functii1.html',
  },
  129: {
    'lname': 'Alarm L',
    'lurl': 'http://www.marplo.net/javascript/curs-jquery-tutoriale-js',
  },
  130: {
    'lname': 'Trip LL',
    'lurl': 'http://www.marplo.net/javascript/jquery-css-js',
  },
  131: {
    'lname': 'Analogue Input 2',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr12_11',
  },
  132: {
    'lname': 'Signal',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr13_12',
  },
  133: {
    'lname': 'Signal Type',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr13_12',
  },
  134: {
    'lname': 'Units',
    'lurl': 'http://coursesweb.net/ajax/',
  },
  135: {
    'lname': 'Signal Limits',
    'lurl': 'http://coursesweb.net/ajax/tutorials_t',
  },
  136: {
    'lname': 'Minimum',
    'lurl': 'http://coursesweb.net/ajax/download_l2',
  },
  137: {
    'lname': 'Maximum',
    'lurl': 'http://coursesweb.net/html/',
  },
  138: {
    'lname': 'Actual Limits',
    'lurl': 'http://coursesweb.net/css/',
  },
  139: {
    'lname': 'Minimum',
    'lurl': 'http://www.marplo.net/',
  },
  140: {
    'lname': 'Maximum',
    'lurl': 'http://www.marplo.net/javascript/',
  },
  141: {
    'lname': 'Alarm Limits',
    'lurl': 'http://www.marplo.net/javascript/lectii-js',
  },
  142: {
    'lname': 'Trip HH',
    'lurl': 'http://www.marplo.net/javascript/sintaxajs.html',
  },
  143: {
    'lname': 'Alarm H',
    'lurl': 'http://www.marplo.net/javascript/functii1.html',
  },
  144: {
    'lname': 'Alarm L',
    'lurl': 'http://www.marplo.net/javascript/curs-jquery-tutoriale-js',
  },
  145: {
    'lname': 'Trip LL',
    'lurl': 'http://www.marplo.net/javascript/jquery-css-js',
  },
  146: {
    'lname': 'Analogue Input 3',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr12_11',
  },
  147: {
    'lname': 'Signal',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr13_12',
  },
  148: {
    'lname': 'Signal Type',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr13_12',
  },
  149: {
    'lname': 'Units',
    'lurl': 'http://coursesweb.net/ajax/',
  },
  150: {
    'lname': 'Signal Limits',
    'lurl': 'http://coursesweb.net/ajax/tutorials_t',
  },
  151: {
    'lname': 'Minimum',
    'lurl': 'http://coursesweb.net/ajax/download_l2',
  },
  152: {
    'lname': 'Maximum',
    'lurl': 'http://coursesweb.net/html/',
  },
  153: {
    'lname': 'Actual Limits',
    'lurl': 'http://coursesweb.net/css/',
  },
  154: {
    'lname': 'Minimum',
    'lurl': 'http://www.marplo.net/',
  },
  155: {
    'lname': 'Maximum',
    'lurl': 'http://www.marplo.net/javascript/',
  },
  156: {
    'lname': 'Alarm Limits',
    'lurl': 'http://www.marplo.net/javascript/lectii-js',
  },
  157: {
    'lname': 'Trip HH',
    'lurl': 'http://www.marplo.net/javascript/sintaxajs.html',
  },
  158: {
    'lname': 'Alarm H',
    'lurl': 'http://www.marplo.net/javascript/functii1.html',
  },
  159: {
    'lname': 'Alarm L',
    'lurl': 'http://www.marplo.net/javascript/curs-jquery-tutoriale-js',
  },
  160: {
    'lname': 'Trip LL',
    'lurl': 'http://www.marplo.net/javascript/jquery-css-js',
  },
  161: {
    'lname': 'Analogue Input 4',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr12_11',
  },
  162: {
    'lname': 'Signal',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr13_12',
  },
  163: {
    'lname': 'Signal Type',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr13_12',
  },
  164: {
    'lname': 'Units',
    'lurl': 'http://coursesweb.net/ajax/',
  },
  165: {
    'lname': 'Signal Limits',
    'lurl': 'http://coursesweb.net/ajax/tutorials_t',
  },
  166: {
    'lname': 'Minimum',
    'lurl': 'http://coursesweb.net/ajax/download_l2',
  },
  167: {
    'lname': 'Maximum',
    'lurl': 'http://coursesweb.net/html/',
  },
  168: {
    'lname': 'Actual Limits',
    'lurl': 'http://coursesweb.net/css/',
  },
  169: {
    'lname': 'Minimum',
    'lurl': 'http://www.marplo.net/',
  },
  170: {
    'lname': 'Maximum',
    'lurl': 'http://www.marplo.net/javascript/',
  },
  171: {
    'lname': 'Alarm Limits',
    'lurl': 'http://www.marplo.net/javascript/lectii-js',
  },
  172: {
    'lname': 'Trip HH',
    'lurl': 'http://www.marplo.net/javascript/sintaxajs.html',
  },
  173: {
    'lname': 'Alarm H',
    'lurl': 'http://www.marplo.net/javascript/functii1.html',
  },
  174: {
    'lname': 'Alarm L',
    'lurl': 'http://www.marplo.net/javascript/curs-jquery-tutoriale-js',
  },
  175: {
    'lname': 'Trip LL',
    'lurl': 'http://www.marplo.net/javascript/jquery-css-js',
  },
  176: {
    'lname': 'Analogue Input 5',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr12_11',
  },
  177: {
    'lname': 'Signal',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr13_12',
  },
  178: {
    'lname': 'Signal Type',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr13_12',
  },
  179: {
    'lname': 'Units',
    'lurl': 'http://coursesweb.net/ajax/',
  },
  180: {
    'lname': 'Signal Limits',
    'lurl': 'http://coursesweb.net/ajax/tutorials_t',
  },
  181: {
    'lname': 'Minimum',
    'lurl': 'http://coursesweb.net/ajax/download_l2',
  },
  182: {
    'lname': 'Maximum',
    'lurl': 'http://coursesweb.net/html/',
  },
  183: {
    'lname': 'Actual Limits',
    'lurl': 'http://coursesweb.net/css/',
  },
  184: {
    'lname': 'Minimum',
    'lurl': 'http://www.marplo.net/',
  },
  185: {
    'lname': 'Maximum',
    'lurl': 'http://www.marplo.net/javascript/',
  },
  186: {
    'lname': 'Alarm Limits',
    'lurl': 'http://www.marplo.net/javascript/lectii-js',
  },
  187: {
    'lname': 'Trip HH',
    'lurl': 'http://www.marplo.net/javascript/sintaxajs.html',
  },
  188: {
    'lname': 'Alarm H',
    'lurl': 'http://www.marplo.net/javascript/functii1.html',
  },
  189: {
    'lname': 'Alarm L',
    'lurl': 'http://www.marplo.net/javascript/curs-jquery-tutoriale-js',
  },
  190: {
    'lname': 'Trip LL',
    'lurl': 'http://www.marplo.net/javascript/jquery-css-js',
  },
  191: {
    'lname': 'Analogue Output',
    'lurl': 'http://www.marplo.net/javascript/jquery-css-js#setcss',
  },
  192: {
    'lname': 'Signal',
    'lurl': 'http://www.marplo.net/javascript/jquery-css-js#arclass',
  },
  193: {
    'lname': 'Signal Type',
    'lurl': 'http://www.marplo.net/javascript/jquery-css-js#ceclass',
  },
  194: {
    'lname': 'Units',
    'lurl': 'http://www.marplo.net/javascript/jquery-fadein-fadeout-js',
  },
  195: {
    'lname': 'Signal Limits',
    'lurl': 'http://www.marplo.net/javascript/jquery-fadein-fadeout-js',
  },
  196: {
    'lname': 'Minimum',
    'lurl': 'http://www.marplo.net/javascript/jquery-fadein-fadeout-js#fadeto',
  },
  197: {
    'lname': 'Maximum',
    'lurl': 'http://www.marplo.net/javascript/tutoriale-js',
  },
  198: {
    'lname': 'Actual Limits',
    'lurl': 'http://coursesweb.net/',
  },
  199: {
    'lname': 'Minimum',
    'lurl': 'http://coursesweb.net/javascript/',
  },
  200: {
    'lname': 'Maximum',
    'lurl': 'http://coursesweb.net/javascript/tutorials_t',
  },
  201: {
    'lname': 'Alarm Limits',
    'lurl': 'http://coursesweb.net/javascript/settimeout-setinterval_t',
  },
  202: {
    'lname': 'Trip HH',
    'lurl': 'http://coursesweb.net/javascript/dynamic-variables-javascript_t',
  },
  203: {
    'lname': 'Alarm H',
    'lurl': 'http://coursesweb.net/javascript/validate-radio-checkbox-buttons_t',
  },
  204: {
    'lname': 'Alarm L',
    'lurl': 'http://coursesweb.net/jquery/jquery-course',
  },
  205: {
    'lname': 'Trip LL',
    'lurl': 'http://coursesweb.net/jquery/jquery-basics',
  },
  206: {
    'lname': 'Simulation',
    'lurl': 'http://coursesweb.net/jquery/jquery-basics#selelm',
  },
  207: {
    'lname': 'Diagnostics',
    'lurl': 'http://coursesweb.net/jquery/jquery-ajax',
  },
  208: {
    'lname': 'Valve Signature Test',
    'lurl': 'http://coursesweb.net/jquery/slidedown-slideup',
  },
  209: {
    'lname': 'Test Setup',
    'lurl': 'http://coursesweb.net/javascript/javascript-scripts_s2',
  },
  210: {
    'lname': 'Step Time',
    'lurl': 'http://coursesweb.net/javascript/trivia-game-script_s2',
  },
  211: {
    'lname': 'Sample Rate',
    'lurl': 'http://coursesweb.net/javascript/trivia-game-script_s2',
  },
  212: {
    'lname': 'Position 1',
    'lurl': 'http://coursesweb.net/php-mysql/',
  },
  213: {
    'lname': 'Position 2',
    'lurl': 'http://coursesweb.net/php-mysql/lessons',
  },
  214: {
    'lname': 'Position 3',
    'lurl': 'http://coursesweb.net/php-mysql/strings',
  },
  215: {
    'lname': 'Position 4',
    'lurl': 'http://coursesweb.net/php-mysql/constants',
  },
  216: {
    'lname': 'Position 5',
    'lurl': 'http://coursesweb.net/php-mysql/arrays',
  },
  217: {
    'lname': 'Position 6',
    'lurl': 'http://coursesweb.net/php-mysql/pdo-introduction-connection-database',
  },
  218: {
    'lname': 'Position 7',
    'lurl': 'http://coursesweb.net/php-mysql/tutorials_t',
  },
  219: {
    'lname': 'Position 8',
    'lurl': 'http://coursesweb.net/php-mysql/uploading-multiple-files_t',
  },
  220: {
    'lname': 'Position 9',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t',
  },
  221: {
    'lname': 'Position 10',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr1_0',
  },
  222: {
    'lname': 'Position 11',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr12_11',
  },
  223: {
    'lname': 'New Test',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr13_12',
  },
  224: {
    'lname': 'Run Test',
    'lurl': 'http://coursesweb.net/php-mysql/common-php-errors-solution_t#perr13_12',
  },
  225: {
    'lname': 'View Test',
    'lurl': 'http://coursesweb.net/ajax/',
  },
  226: {
    'lname': 'Save Test',
    'lurl': 'http://coursesweb.net/ajax/tutorials_t',
  },
  227: {
    'lname': 'Save Test as Master',
    'lurl': 'http://coursesweb.net/ajax/download_l2',
  },
  228: {
    'lname': 'Clear Test Data',
    'lurl': 'http://coursesweb.net/html/',
  },
  229: {
    'lname': 'View Previous Test',
    'lurl': 'http://coursesweb.net/css/',
  },
  230: {
    'lname': 'View Master Test',
    'lurl': 'http://www.marplo.net/',
  },
  231: {
    'lname': 'View Data',
    'lurl': 'http://www.marplo.net/javascript/',
  },
  232: {
    'lname': 'Hysteresis (max)',
    'lurl': 'http://www.marplo.net/javascript/lectii-js',
  },
  233: {
    'lname': 'Master',
    'lurl': 'http://www.marplo.net/javascript/sintaxajs.html',
  },
  234: {
    'lname': 'Previous Test',
    'lurl': 'http://www.marplo.net/javascript/functii1.html',
  },
  235: {
    'lname': 'Overshoot (max)',
    'lurl': 'http://www.marplo.net/javascript/curs-jquery-tutoriale-js',
  },
  236: {
    'lname': 'Master',
    'lurl': 'http://www.marplo.net/javascript/jquery-css-js',
  },
  237: {
    'lname': 'Previous Test',
    'lurl': 'http://www.marplo.net/javascript/jquery-css-js#setcss',
  },
  238: {
    'lname': 'About',
    'lurl': 'http://www.marplo.net/javascript/jquery-css-js#arclass',
  },
  239: {
    'lname': 'I/O Code',
    'lurl': 'http://www.marplo.net/javascript/jquery-css-js#ceclass',
  },
  240: {
    'lname': 'Serial Number',
    'lurl': 'http://www.marplo.net/javascript/jquery-fadein-fadeout-js',
  },
  241: {
    'lname': 'Software Release',
    'lurl': 'http://www.marplo.net/javascript/jquery-fadein-fadeout-js',
  },
  242: {
    'lname': 'Date of Manufacture',
    'lurl': 'http://www.marplo.net/javascript/jquery-fadein-fadeout-js#fadeto',
  },
  243: {
    'lname': 'Test Setup',
    'lurl': 'http://www.marplo.net/javascript/jquery-fadein-fadeout-js#fadeto',
  }
};
